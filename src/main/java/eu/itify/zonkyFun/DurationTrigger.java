package eu.itify.zonkyFun;

import java.util.Date;

import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.TriggerContext;
import org.springframework.stereotype.Service;

@Service
public class DurationTrigger implements Trigger {
	
	private Date startTime;
	private Date endTime;
	
	private volatile boolean fixedRate = false;
	
	private long period;
	private Date lastScheduledExecutionTime = null;
	
	public DurationTrigger(Date startTime, Date endTime, long period, boolean fixedRate) {
		this.startTime =  startTime;
		this.endTime = endTime;
		this.period = period;
		this.fixedRate = fixedRate;
	}
	
	public DurationTrigger() {
		
	}

	@Override
	public Date nextExecutionTime(TriggerContext triggerContext) {
		long now = System.currentTimeMillis();
		
		if (triggerContext.lastActualExecutionTime() != null) {
			this.lastScheduledExecutionTime = triggerContext.lastActualExecutionTime();
		}

		if ( now < this.endTime.getTime() ) {

			if ( now >= this.startTime.getTime() ) {
				if ( ! this.fixedRate ) {
					if (triggerContext.lastScheduledExecutionTime() != null) {
						return new Date( triggerContext.lastCompletionTime().getTime() + this.period );
					} else {
						return new Date( this.startTime.getTime() + this.period);
					}
				}
				else {
					if (triggerContext.lastScheduledExecutionTime() != null) {
						return new Date( triggerContext.lastScheduledExecutionTime().getTime() + this.period );
					} else {
						return new Date( this.startTime.getTime() + this.period);
					}
				}
			} else {
				return new Date( now + ( this.startTime.getTime() - now ) );
			}
		}
	
		return null;
	}
	
	public Date getPreviousTime() {
		
		return this.lastScheduledExecutionTime;
	
	}
	
 }