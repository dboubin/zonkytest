package eu.itify.zonkyFun;

import java.io.IOException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.scheduling.annotation.EnableScheduling;

import com.fasterxml.jackson.core.JsonParseException;

import com.fasterxml.jackson.databind.JsonMappingException;

import eu.itify.zonkyFun.service.TriggerLoanService;
import eu.itify.zonkyFun.service.impl.TriggerLoanServiceImpl;


@SpringBootApplication
//@EnableScheduling				// just to show another way... in this case remove all triggerLoanService calls bellow
public class ZonkyFunApplication {

	
	public static void main(String[] args) throws JsonParseException, JsonMappingException, IOException {
		SpringApplication.run(ZonkyFunApplication.class, args);
	
		TriggerLoanService triggerLoanService = new TriggerLoanServiceImpl();
		
		triggerLoanService.processNewLoans();
		
		System.out.println("----finished----");
	}

}
