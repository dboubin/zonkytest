package eu.itify.zonkyFun.service;

import java.io.IOException;
import java.time.Instant;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import eu.itify.zonkyFun.service.dto.LoanDTO;

public interface LoanService extends Runnable {
	
	@Override
	public void run();

	public List<LoanDTO> initLoanProcessing() throws JsonParseException, JsonMappingException, IOException;
	
	public List<LoanDTO> processNewLoans(Instant time) throws JsonParseException, JsonMappingException, IOException;
	
	public List<LoanDTO> processNewLoans() throws JsonParseException, JsonMappingException, IOException;
	
	public void printLoans(List<LoanDTO> loans, String params);
	
	public void printHeader(Instant infoLastTime);

	
}
