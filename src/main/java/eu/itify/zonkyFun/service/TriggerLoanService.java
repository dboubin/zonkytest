package eu.itify.zonkyFun.service;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

public interface TriggerLoanService {

	public void processNewLoans() throws JsonParseException, JsonMappingException, IOException;
	
	
}
