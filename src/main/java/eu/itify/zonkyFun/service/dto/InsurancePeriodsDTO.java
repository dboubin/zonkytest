package eu.itify.zonkyFun.service.dto;

import java.time.Instant;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class InsurancePeriodsDTO {
	private Instant policyPeriodFrom;
	private Instant policyPeriodTo;
	
	@JsonIgnore
	public Instant getPolicyPeriodFrom() {
		return policyPeriodFrom;
	}
	public void setPolicyPeriodFrom(Instant policyPeriodFrom) {
		this.policyPeriodFrom = policyPeriodFrom;
	}

	@JsonIgnore
	public Instant getPolicyPeriodTo() {
		return policyPeriodTo;
	}
	public void setPolicyPeriodTo(Instant policyPeriodTo) {
		this.policyPeriodTo = policyPeriodTo;
	}
	@Override
	public String toString() {
		return "InsurancePeriodsDTO [policyPeriodFrom=" + policyPeriodFrom + ", policyPeriodTo=" + policyPeriodTo + "]";
	}
	
	
}
