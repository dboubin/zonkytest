package eu.itify.zonkyFun.service.dto;

import eu.itify.zonkyFun.service.dto.PhotoDTO;

public class LoanDTO {
	private Long id;
	private String url;
	private String name;
	private String story;
	private String purpose;
	private PhotoDTO[] photos;
	private Long userId;
	private String nickName;
	private Long termInMonths;
	private Double interestRate;
	private Double revenueRate;
	private Double annuity;
	private Long premium;
	private String rating;
	private boolean topped;
	private Double amount;
	private Double remainingInvestment;
	private Double investmentRate;
	private boolean covered;
	private Double reservedAmount;
	private Double zonkyPlusAmount;
	
	private String datePublished;
	
	private boolean published;

	private String deadline;
	
	private String myOtherInvestments;
	private String borrowerRelatedInvestmentInfo;
	private Long investmentsCount;
	private Long questionsCount;
	private Long region;
	private String mainIncomeType;
	private boolean questionsAllowed;
	private Long activeLoansCount;
	private boolean insuranceActive;
	private InsurancePeriodsDTO[] insuranceHistory;
	private boolean fastcash;
	private boolean multicash;
	private Double annuityWithInsurance;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStory() {
		return story;
	}
	public void setStory(String story) {
		this.story = story;
	}
	public String getPurpose() {
		return purpose;
	}
	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public Long getTermInMonths() {
		return termInMonths;
	}
	public void setTermInMonths(Long termInMonths) {
		this.termInMonths = termInMonths;
	}
	public Double getInterestRate() {
		return interestRate;
	}
	public void setInterestRate(Double interestRate) {
		this.interestRate = interestRate;
	}
	public Double getRevenueRate() {
		return revenueRate;
	}
	public void setRevenueRate(Double revenueRate) {
		this.revenueRate = revenueRate;
	}
	public Double getAnnuity() {
		return annuity;
	}
	public void setAnnuity(Double annuity) {
		this.annuity = annuity;
	}
	public Long getPremium() {
		return premium;
	}
	public void setPremium(Long premium) {
		this.premium = premium;
	}
	public String getRating() {
		return rating;
	}
	public void setRating(String rating) {
		this.rating = rating;
	}
	public boolean isTopped() {
		return topped;
	}
	public void setTopped(boolean topped) {
		this.topped = topped;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Double getRemainingInvestment() {
		return remainingInvestment;
	}
	public void setRemainingInvestment(Double remainingInvestment) {
		this.remainingInvestment = remainingInvestment;
	}
	public Double getInvestmentRate() {
		return investmentRate;
	}
	public void setInvestmentRate(Double investmentRate) {
		this.investmentRate = investmentRate;
	}
	public boolean isCovered() {
		return covered;
	}
	public void setCovered(boolean covered) {
		this.covered = covered;
	}
	public Double getReservedAmount() {
		return reservedAmount;
	}
	public void setReservedAmount(Double reservedAmount) {
		this.reservedAmount = reservedAmount;
	}
	public Double getZonkyPlusAmount() {
		return zonkyPlusAmount;
	}
	public void setZonkyPlusAmount(Double zonkyPlusAmount) {
		this.zonkyPlusAmount = zonkyPlusAmount;
	}
	
	public String getDatePublished() {
		return datePublished;
	}
	public void setDatePublished(String datePublished) {
		this.datePublished = datePublished;
	}
	

	public boolean isPublished() {
		return published;
	}
	public void setPublished(boolean published) {
		this.published = published;
	}
	public String getDeadline() {
		return deadline;
	}
	public void setDeadline(String deadline) {
		this.deadline = deadline;
	}
	public String getMyOtherInvestments() {
		return myOtherInvestments;
	}
	public void setMyOtherInvestments(String myOtherInvestments) {
		this.myOtherInvestments = myOtherInvestments;
	}
	public String getBorrowerRelatedInvestmentInfo() {
		return borrowerRelatedInvestmentInfo;
	}
	public void setBorrowerRelatedInvestmentInfo(String borrowerRelatedInvestmentInfo) {
		this.borrowerRelatedInvestmentInfo = borrowerRelatedInvestmentInfo;
	}
	public Long getInvestmentsCount() {
		return investmentsCount;
	}
	public void setInvestmentsCount(Long investmentsCount) {
		this.investmentsCount = investmentsCount;
	}
	public Long getQuestionsCount() {
		return questionsCount;
	}
	public void setQuestionsCount(Long questionsCount) {
		this.questionsCount = questionsCount;
	}
	public Long getRegion() {
		return region;
	}
	public void setRegion(Long region) {
		this.region = region;
	}
	public String getMainIncomeType() {
		return mainIncomeType;
	}
	public void setMainIncomeType(String mainIncomeType) {
		this.mainIncomeType = mainIncomeType;
	}
	public boolean isQuestionsAllowed() {
		return questionsAllowed;
	}
	public void setQuestionsAllowed(boolean questionsAllowed) {
		this.questionsAllowed = questionsAllowed;
	}
	public Long getActiveLoansCount() {
		return activeLoansCount;
	}
	public void setActiveLoansCount(Long activeLoansCount) {
		this.activeLoansCount = activeLoansCount;
	}
	public boolean isInsuranceActive() {
		return insuranceActive;
	}
	public void setInsuranceActive(boolean insuranceActive) {
		this.insuranceActive = insuranceActive;
	}
	public boolean isFastcash() {
		return fastcash;
	}
	public void setFastcash(boolean fastcash) {
		this.fastcash = fastcash;
	}
	public boolean isMulticash() {
		return multicash;
	}
	public void setMulticash(boolean multicash) {
		this.multicash = multicash;
	}
	public Double getAnnuityWithInsurance() {
		return annuityWithInsurance;
	}
	public void setAnnuityWithInsurance(Double annuityWithInsurance) {
		this.annuityWithInsurance = annuityWithInsurance;
	}
	public PhotoDTO[] getPhotos() {
		return photos;
	}
	public void setPhotos(PhotoDTO[] photos) {
		this.photos = photos;
	}
	public InsurancePeriodsDTO[] getInsuranceHistory() {
		return insuranceHistory;
	}
	public void setInsuranceHistory(InsurancePeriodsDTO[] insuranceHistory) {
		this.insuranceHistory = insuranceHistory;
	}
	@Override
	public String toString() {
		return "id=" + id + ", url=" + url + ", name=" + name + ", story=" + story + ", purpose=" + purpose
				+ ", userId=" + userId + ", nickName=" + nickName + ", termInMonths="
				+ termInMonths + ", interestRate=" + interestRate + ", revenueRate=" + revenueRate + ", annuity="
				+ annuity + ", premium=" + premium + ", rating=" + rating + ", topped=" + topped + ", amount=" + amount
				+ ", remainingInvestment=" + remainingInvestment + ", investmentRate=" + investmentRate + ", covered="
				+ covered + ", reservedAmount=" + reservedAmount + ", zonkyPlusAmount=" + zonkyPlusAmount
				+ ", datePublished=" + datePublished + ", published=" + published + ", deadline=" + deadline
				+ ", myOtherInvestments=" + myOtherInvestments + ", borrowerRelatedInvestmentInfo="
				+ borrowerRelatedInvestmentInfo + ", investmentsCount=" + investmentsCount + ", questionsCount="
				+ questionsCount + ", region=" + region + ", mainIncomeType=" + mainIncomeType + ", questionsAllowed="
				+ questionsAllowed + ", activeLoansCount=" + activeLoansCount + ", insuranceActive=" + insuranceActive
				+ ", fastcash=" + fastcash + ", multicash=" + multicash
				+ ", annuityWithInsurance=" + annuityWithInsurance + "]";
	}	
	
	
	
}
