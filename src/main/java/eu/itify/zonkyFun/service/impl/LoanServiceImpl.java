package eu.itify.zonkyFun.service.impl;

import java.io.IOException;
import java.net.URL;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import eu.itify.zonkyFun.DurationTrigger;
import eu.itify.zonkyFun.service.dto.LoanDTO;
import eu.itify.zonkyFun.service.LoanService;

@Service
public class LoanServiceImpl implements Runnable, LoanService {
	// this would also be wise have in a config file:
	private String url="https://api.zonky.cz/loans/marketplace";
	
	private final DurationTrigger trigger;
	private Instant lastRun = null;
	
	public LoanServiceImpl(DurationTrigger trigger) {
		this.trigger = trigger;
	}
	
	public List<LoanDTO> initLoanProcessing() throws JsonParseException, JsonMappingException, IOException {
		
		List<LoanDTO> usrLoans = new ArrayList<>();
		usrLoans = this.processNewLoans(null);
		
		return usrLoans;
	}
	
	public List<LoanDTO> processNewLoans(Instant time) throws JsonParseException, JsonMappingException, IOException {
		
		List<LoanDTO> usrLoans = new ArrayList<>();
	
		String parametrizedUrl = this.url + "?datePublished__gte=";
		if (time != null) {
			parametrizedUrl += time.toString();
		} else {
			parametrizedUrl += Instant.now().minusSeconds(300); 	// lets start 5 mins before now (yes, better to read from a config file)
		}
		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());
		try {
			usrLoans = mapper.readValue(new URL(parametrizedUrl), new TypeReference<ArrayList<LoanDTO>>() {});
		} catch (IOException e) {
			e.printStackTrace();
		}

		return usrLoans;
	}
	
	public List<LoanDTO> processNewLoans() throws JsonParseException, JsonMappingException, IOException {
		
		Instant time = null; 
		if (this.trigger.getPreviousTime() !=null) {
			time = this.trigger.getPreviousTime().toInstant();
		}
		
		List<LoanDTO> usrLoans = new ArrayList<>();
	
		String parametrizedUrl = this.url + "?datePublished__gte=";
		if (time != null) {
			parametrizedUrl += time.toString();
		} else {
			Instant instant = Instant.now().minusSeconds(300); 	// lets start 5 mins before now (yes, better to read from a config file)
			parametrizedUrl += instant;
		}
		
		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());
		try {
			usrLoans = mapper.readValue(new URL(parametrizedUrl), new TypeReference<ArrayList<LoanDTO>>() {});
		} catch (IOException e) {
			e.printStackTrace();
		}

		return usrLoans;
	}
	
	@Override
	public void run() {
		
		Instant infoLastTime = this.lastRun;

		this.printHeader(infoLastTime);
		try {
			this.printLoans(this.processNewLoans(), "id,datePublished,nickName,name");
//			this.printLoans(this.processNewLoans(), "");	// use to get all fields
			this.lastRun = Instant.now();
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public void printLoans(List<LoanDTO> loans, String params) {
		String printedLine = "";
		ObjectMapper objMapper = new ObjectMapper();
		objMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		
		for (LoanDTO item: loans) {
			@SuppressWarnings("unchecked")
			Map<String, Object> itemMap = objMapper.convertValue(item, Map.class);
			
			// print accordingly regarding chosen fields
			if (params.length() > 0) {
				String[] paramsArray = params.split(",");
				if (itemMap.containsKey(paramsArray[0])) {
					printedLine = paramsArray[0] + "=" + itemMap.get(paramsArray[0]).toString();
				} else {
					printedLine = "wrongArg:" + paramsArray[0];
				}
				if (paramsArray.length > 1) {
					for (int i = 1; i < paramsArray.length; i++) {
						if (itemMap.containsKey(paramsArray[i])) {
							printedLine += ", " + paramsArray[i] + "=" + itemMap.get(paramsArray[i]).toString();
						} else {
							printedLine += " - wrongArg:" + paramsArray[i];
						}
					}
				}
			} else {
				printedLine = item.toString();		// or print all fields
			}
			System.out.println(printedLine);
		}
	
	}
	
	
	public void printHeader(Instant infoLastTime) {
		if (infoLastTime == null) {
			System.out.println("========== Init ================= " + DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(ZonedDateTime.now()).toString());
		} else {
			System.out.println("========== " + DateTimeFormatter.ISO_OFFSET_DATE_TIME.withZone(ZoneId.systemDefault()).format(infoLastTime).toString() + " ================= " + DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(ZonedDateTime.now()).toString());
		}
	}

}
