package eu.itify.zonkyFun.service.impl;

import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import eu.itify.zonkyFun.service.dto.LoanDTO;

@Component
public class SchedulerLoanComponent {

	private static final Logger log = LoggerFactory.getLogger(SchedulerLoanComponent.class);
	
	private Instant lastRun = null;
	
	@Autowired
	private LoanServiceImpl loanService;
	
	
	@Scheduled(fixedRate = 300000)
	public void reportCurrentTime() throws JsonParseException, JsonMappingException, IOException {
		List<LoanDTO> usrLoans = new ArrayList<>();
		Instant infoLastTime = this.lastRun;
		this.lastRun = Instant.now();
		
		if (infoLastTime == null) {
			usrLoans = loanService.initLoanProcessing();
		} else {
			usrLoans = loanService.processNewLoans(infoLastTime);
		}
		this.lastRun = Instant.now();
		
		loanService.printHeader(infoLastTime);
		loanService.printLoans(usrLoans, "");
		
		
	}

}
