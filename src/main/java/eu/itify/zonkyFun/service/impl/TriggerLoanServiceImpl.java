package eu.itify.zonkyFun.service.impl;

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.ScheduledFuture;

import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import eu.itify.zonkyFun.service.TriggerLoanService;
import eu.itify.zonkyFun.DurationTrigger;

@Service
public class TriggerLoanServiceImpl implements TriggerLoanService {
	
	// it would be wise to put these into a config file:
	private Date startTime = new Date(System.currentTimeMillis() + 5000);
	private Date endTime = new Date(System.currentTimeMillis() + 24*60*60*1000 + 5000);		// run for next 24 hours
	private long period = 300000l;				// set the period here 300000 = 5min
	private boolean fixedRate = true;
	
	public TriggerLoanServiceImpl() {
		
	}
	
	public void processNewLoans() throws JsonParseException, JsonMappingException, IOException {

		DurationTrigger trigger = new DurationTrigger(this.startTime, this.endTime, this.period, this.fixedRate);
		LoanServiceImpl runnableLoanProcessTask = new LoanServiceImpl(trigger);
		ThreadPoolTaskScheduler taskScheduler = new ThreadPoolTaskScheduler();
		taskScheduler.initialize();
		
		ScheduledFuture<?> task = taskScheduler.schedule(runnableLoanProcessTask, trigger);
		
		while (!task.isDone()) {
			
		};
		
		taskScheduler.destroy();
		System.out.println("--- Scheduler task cleared ---");
		
	}

	
}
